#!/usr/bin/env python3

import os
import re
import json
import sys
import signal
from threading import Thread
import subprocess
from subprocess import Popen, PIPE, STDOUT
from queue import Queue, Empty
from time import sleep
import configparser

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, GLib

class Friso:
    def __init__(self):
        # my vars
        self.chosen_partition_func = False
        self.chosen_partition = False
        self.main_selection = False
        self.disk_selection = False
        self.disks = []
        self.partitions = {}
        self.queue = Queue()
        self.config = configparser.ConfigParser()
        self.config_ini = 'settings.ini'
        self.config.read(self.config_ini)
        self.copy_pid = False
        self.run_after_thread = False
        self.mnt_dir = None
        self.build_dir = None
        self.kversion = None
        self.stop_progress_bar = False
        self.type_progress_bar = 'rsync'

        # gtk stuff
        self.builder = Gtk.Builder()
        self.builder.add_from_file(os.getcwd()+"/ui.glade")
        self.builder.connect_signals(self)

        # gtk css stuff
        screen = Gdk.Screen.get_default()
        provider = Gtk.CssProvider()
        provider.load_from_path(os.getcwd()+"/style.css")
        Gtk.StyleContext.add_provider_for_screen(screen, provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

        # show main window
        self.window = self.builder.get_object("main_window")
        self.window.show()


    # main window destroy
    def on_main_window_destroy(self, window):
        Gtk.main_quit()


    # hide other windows instead of destroying
    def on_window_delete(self, window, *data):
        window.hide()
        return True


    # get kernel version from self.mnt_dir (so needs to be mounted already)
    def get_kversion(self):
        cmd = "cd " + self.mnt_dir + "/boot && ls -1 vmlinuz-* | tail -1 | sed 's@vmlinuz-@@'"
        p = subprocess.run(cmd, capture_output=True, text=True, shell=True)

        if p.stderr:
            print('ERROR: cant find kversion...')
            return False

        if p.stdout:
            return p.stdout.strip()

        return False


    # main window list
    def main_list_changed(self, list):
        if self.main_selection == False: # ignore Gtk's first selection
            self.main_selection = True
            return

        self.main_window_hide_all_objects()

        # see what's selected
        model, treeiter = list.get_selected()
        if treeiter is not None:
            selection = model[treeiter][1]
            print('selection', selection)

        if selection == "backup_os":
            self.builder.get_object('backup_os_box').show()

        if selection == "partition_disk":
            self.show_partitions_window('partition')

        if selection == "settings":
            tmp_dir = self.config.get('DEFAULT', 'tmp_dir', fallback='/tmp')
            self.builder.get_object("tmp_dir").set_text(tmp_dir)
            self.builder.get_object("settings_box").show()

    # hide all object except list
    def main_window_hide_all_objects(self):
        self.builder.get_object('intro_text').hide()
        self.builder.get_object("backup_os_box").hide()
        self.builder.get_object("settings_box").hide()


    # save settings to .ini file    
    def settings_save(self, btn):
        # get tmp dir from input
        tmp_dir = self.builder.get_object("tmp_dir").get_text()
        self.config['DEFAULT']['tmp_dir'] = tmp_dir

        print('saving to ' + self.config_ini)
        with open(self.config_ini, 'w') as configfile:
            self.config.write(configfile)
        

    # main window about btn was clicked
    def about_clicked(self, button):
        about = self.builder.get_object("about_window")
        about.show()


    # main window quit click
    def quit_clicked(self, button):
        Gtk.main_quit()


    # rsync / mksquashfs animate progress bar
    def update_progress_bar(self):
        if self.stop_progress_bar:
            print('timer should stop')
            self.stop_progress_bar = False
            return False

        bar = self.builder.get_object("copy_progress_bar")

        try: line = self.queue.get_nowait()
        except Empty:
            # print('no output yet')
            return True
        else:
            line = line.strip()

            if not line or line.find('] ') == -1:
                return True

            # we're now processing the CLI progress bar
            # self.builder.get_object("copy_spinner").hide()
            tmp = line.split('] ')
            parts = tmp[1].strip().split()

            if self.type_progress_bar == 'mksquashfs':
                completed = parts[0].strip()
                percent = parts[1].strip()
            else:
                completed = parts[1].strip()
                percent = parts[0].strip()
                
            # print('type', self.type_progress_bar, 'parts', parts, 'percent', percent)

            if not percent:
                return True

            percent = percent.replace('%', '')
            fraction = int(percent) / 100
            bar.set_fraction(fraction)
            bar.set_text('Completed ' + completed.strip() + ' (' + percent + '%)')
        
        return True


    # clear queue every time so we dont fall behind when showing progress
    def enqueue_output(self, out, queue):
        print('enqueue_output')
        with queue.mutex:
            queue.queue.clear()

        for line in out:
            queue.put(line)

        print('enqueue end.')
        out.close()

    
    # start thread on command
    def thread_start(self, cmd):
        ON_POSIX = 'posix' in sys.builtin_module_names
        p = Popen(cmd, shell = True, text = True, stdout=PIPE, preexec_fn=os.setsid, close_fds=ON_POSIX)#, bufsize=0)
        t = Thread(target=self.enqueue_output, args=(p.stdout, self.queue))
        self.copy_pid = p.pid
        t.daemon = True # thread dies with the program
        t.start()
        signal.signal(signal.SIGCHLD, self.thread_completed)


    # is called when linux CLI cmd ran
    def thread_completed(self, signum, frame):
        if self.run_after_thread is False:
            pass
    
        print('thread completed')
        func = getattr(self, self.run_after_thread)

        self.stop_progress_bar = True
        self.run_after_thread = False
        func()


    # mount dir
    def mount(self, from_dev, to_dir):
        cmd = 'mount ' + from_dev + ' ' + to_dir
        # print('executing: ' + cmd)
    
        proc = subprocess.run(cmd, text = True, shell=True, stdout=PIPE, stderr=PIPE)
        # print('returncode', proc.returncode, 'stdout', proc.stdout, 'stderr', proc.stderr, 'find', proc.stderr.find('alaaready mounted'))

        if proc.returncode == 0:
            return True

        if proc.stderr.find('already mounted on ' + to_dir) != -1:
            # print('already mounted (is ok)')
            return True

        return False


    # when a disk is pressed in device_list
    def device_list_changed(self, list):
        if self.disk_selection == False:
            self.disk_selection = True
            return

        model, treeiter = list.get_selected()
        if treeiter is not None:
            # probe partitions on disk and add to store
            disk_dev = model[treeiter][0]
            self.probe_partitions(disk_dev)
            self.add_partitions_to_store()


    # collect partition info and put in self.partitions
    def probe_partitions(self, disk_dev = '/dev/sda'):
        self.partitions = {}

        # get info via sfdisk
        txt = subprocess.check_output(['sfdisk', '-n', '-J', '-d', disk_dev]).decode()
        obj = json.loads(txt)

        for partition in obj['partitiontable']['partitions']:
            if not partition: # ignore empty strings
                continue

            obj = partition
            obj['end'] = partition['start'] + partition['size']
            dev = partition['node']
            self.partitions[dev] = obj

        # get labels from blkid
        for partition in subprocess.check_output('blkid | grep ' + disk_dev, shell=True).decode().split('\n'):
            if not partition:
                continue

            tmp = partition.split(': ')
            dev = tmp[0].strip()

            self.partitions[dev]['node'] = dev
            self.partitions[dev]['type'] = ''
            if (tmp[1].find('TYPE=') != -1):
                self.partitions[dev]['type'] = re.findall(r'TYPE="(.*?)"', tmp[1])[0]

            self.partitions[dev]['label'] = ''
            if (tmp[1].find('LABEL=') != -1):
                self.partitions[dev]['label'] = re.findall(r'LABEL="(.*?)"', tmp[1])[0]


    # put data (partitions) in rightside list
    def add_partitions_to_store(self):
        store = self.builder.get_object("partition_list_store")
        store.clear()

        for partition in self.partitions:
            obj = self.partitions[partition]
            arr = [obj['node'], obj['type'], str(obj['start']), str(obj['end']), str(obj['size']), obj['label'], ]
            store.append(arr)


    # scan for disks an put in self.disks array 
    def probe_disks(self):
        self.disks = []
        disks = subprocess.check_output('ls /dev/sd?', shell=True).decode().split('\n')

        for disk in disks:
            if not disk:
                continue

            self.disks.append(disk)


    # put data (disks) in leftside list 
    def add_disks_to_store(self):
        store = self.builder.get_object("device_list_store")

        for disk in self.disks:
            store.append([disk, ])


    # edit or select a partition (on a disk)
    def show_partitions_window(self, show_action = 'select'):
        self.probe_disks()
        self.add_disks_to_store()

        # select last disk since that is probably the USB
        # when a change in the list happens the partition probing starts
        last = len(self.disks) - 1
        self.builder.get_object("device_list").set_cursor(last)

        self.probe_partitions(self.disks[last])
        self.add_partitions_to_store()

        # finally show partitions_window
        if show_action == 'select':
            self.builder.get_object("partition_actions_box").hide()
            self.builder.get_object("select_actions_box").show()

        if show_action == 'partition':
            self.builder.get_object("partition_actions_box").show()
            self.builder.get_object("select_actions_box").hide()
        
        self.builder.get_object("partitions_window").show()


    # cancel btn was clicked
    def partitions_window_cancel_btn(self, btn):
        partitions_window = self.builder.get_object("partitions_window")
        partitions_window.hide_on_delete()


    # open window where you can choose the location for a new file
    def backup_file_select_btn(self, btn):
        new_file_window = self.builder.get_object("new_file_location_window")
        new_file_window.show()


    # selection window to where to save a file to, cancel btn
    def new_file_location_window_cancel_btn(self, btn):
        window = self.builder.get_object("new_file_location_window")
        window.hide_on_delete()


    # save logic for a selection window to where to save a file to, save btn
    def new_file_location_window_save_btn(self, btn):
        # get file path & close window
        window = self.builder.get_object("new_file_location_window")
        filepath = window.get_filename()
        window.hide_on_delete()

        # put selected filepath to input / entry element
        self.builder.get_object("backup_filepath").set_text(filepath)


    # error window
    def error_window_show(self, txt = 'An error occurred', title = 'Error'):
        error_window = self.builder.get_object("error_window")
        error_window.set_property("text", title)
        error_window.set_property("secondary_text", txt)
        error_window.show()


    # error window
    def error_window_close(self, window, btn):
        window.hide()


    # warning window / confirm window
    def warning_window_response(self, window, btn):
        window.hide()

        if btn == -9: # no
            self.builder.get_object("backup_filepath").set_text('')

        if btn == -8: # yes
            self.backup_partition_logic()


    # show partition selection window
    def backup_partition_btn_clicked(self, btn):
        filepath = self.builder.get_object("backup_filepath").get_text()
        
        filepath = filepath + '.iso'

        if not filepath:
            self.error_window_show("No iso save filepath chosen.")
        elif not os.path.dirname(filepath):
            self.error_window_show("The iso save target directory does no exist.")
        elif os.path.exists(filepath):
            # overwrite?
            self.builder.get_object("confirm_window").show()
        else:
            # all is ok, select a partition.
            self.chosen_partition_func = 'backup_partition_logic'
            self.show_partitions_window('select')


    # save self.chosen_partition when a partition is selected in the partitions window
    def partition_select_btn_clicked(self, btn):
        self.builder.get_object("partitions_window").hide()

        selected_index = self.builder.get_object("partition_list").get_cursor() 
        store = self.builder.get_object("partition_list_store")
        self.chosen_partition = store[selected_index[0]][0]

        # call saved func var
        func = getattr(self, self.chosen_partition_func)
        func()
        # self.backup_partition_logic()



    # a list that will be shown in the copy_info_label. it changes when an item is running
    def update_copy_info_label(self, working_index = 0):
        labels =[
                    { 
                        'todo': "Calculate system size.",
                        'working': 'Calculating system size...',
                        'done': 'Calculated system size.'
                    },
                    { 
                        'todo': "Generate md5.",
                        'working': 'Generating md5...',
                        'done': 'Generated md5.'
                    },
                    {
                        'todo': "Copy vmlinuz.",
                        'working': 'Copying vmlinuz...',
                        'done': 'Copied vmlinuz.'
                    },
                    {
                        'todo': "Copy initrd.img.",
                        'working': 'Copying initrd.img...',
                        'done': 'Copied initrd.img.'
                    },
                    {
                        'todo': "Make squash filesystem.",
                        'working': 'Making squash filesystem...',
                        'done': 'Made squash filesystem.'
                    },
                    {
                        'todo': "Make iso file.",
                        'working': 'Making iso file...',
                        'done': 'Made iso file.'
                    },
                ]

        txt = ''
        for index, obj in enumerate(labels):
            no = index + 1
            if index < working_index:
                txt += str(no) + ". " + obj['done'] + "\n"

            if index == working_index:
                txt += str(no) + ". " + obj['working'] + "\n"

            if index > working_index:
                txt += str(no) + ". " + obj['todo'] + "\n"

        self.builder.get_object("copy_info_label").set_text(txt)



    # gets called after partition selection
    def backup_partition_logic(self):
        self.builder.get_object("copy_window").show()

        # collect all files for livecd in a tmp dir
        tmp_dir = self.config.get('DEFAULT', 'tmp_dir', fallback='/tmp')
        self.build_dir = tmp_dir + '/friso_bootableUSB'
        os.makedirs(self.build_dir+'/casper', exist_ok=True)

        self.mnt_dir = self.config.get('DEFAULT', 'mnt_dir', fallback='/mnt/friso')
        os.makedirs(self.mnt_dir, exist_ok=True)

        # mount chosen partition
        if not self.mount(self.chosen_partition, self.mnt_dir):
            print('could not mount')
            quit()

        self.kversion = self.get_kversion()
        self.calculate_filesystem_size()


    # 1. calculate filesystem size
    def calculate_filesystem_size(self):
        self.update_copy_info_label(0)

        bar = self.builder.get_object("copy_progress_bar")
        bar.set_fraction(0.25)
        bar.set_text('Calculating system size...')
        sleep(2)

        # create filesystem.size so ubiquity installer can see if you got enough space upon install
        cmd = "echo -n $(sudo du -s --block-size=1 " + self.mnt_dir + " | tail -1 | awk '{print $1}') | sudo tee " + self.build_dir + "/filesystem.size"
        print('exec', cmd)
        self.run_after_thread = 'generate_md5'
        self.thread_start(cmd)


    # 2. generate md5
    def generate_md5(self):
        self.update_copy_info_label(1)

        bar = self.builder.get_object("copy_progress_bar")
        bar.set_fraction(0.50)
        bar.set_text('Generating md5...')
        sleep(2)
        
        cmd = 'find ' + self.build_dir + ' -type f -print0 | xargs -0 md5sum | sed "s@' + self.build_dir + '@.@" | grep -v md5sum.txt | sudo tee -a ' + self.build_dir + '/md5sum.txt'
        print('exec', cmd)
        self.run_after_thread = 'copy_vmlinuz'
        self.thread_start(cmd)

    
    # 3. copy vmlinuz
    def copy_vmlinuz(self):
        self.update_copy_info_label(2)
        self.builder.get_object("copy_spinner").show()
        self.builder.get_object("copy_progress_bar").set_text('Copying vmlinuz...')

        vmlinuz_from = self.mnt_dir+'/boot/vmlinuz-'+self.kversion
        vmlinuz_to = self.build_dir+'/casper/vmlinuz'

        cmd = "rsync -aW --no-inc-recursive --info=progress2 " + vmlinuz_from + " " + vmlinuz_to
        print('exec', cmd)
        sleep(2)

        GLib.timeout_add(300, self.update_progress_bar)
        self.run_after_thread = 'copy_initrd'
        self.thread_start(cmd)
    

    # 4. copy initrd (on fedora its initramfs-kversion.img)
    def copy_initrd(self):
        self.update_copy_info_label(3)
        self.builder.get_object("copy_spinner").show()
        self.builder.get_object("copy_progress_bar").set_text('Copying initrd...')

        # for ubuntu
        initrd_from = self.mnt_dir + '/boot/initrd.img-'+self.kversion
        initrd_to = self.build_dir + '/casper/initrd.img'

        if not os.path.exists(initrd_from):
            # for fedora
            initrd_from = self.mnt_dir + '/boot/initramfs-'+self.kversion+'.img'
            initrd_to = self.build_dir + '/casper/initramfs.img'

        cmd = "rsync -aW --no-inc-recursive --info=progress2 " + initrd_from + " " + initrd_to
        print('exec', cmd)
        sleep(1)

        GLib.timeout_add(300, self.update_progress_bar)
        self.run_after_thread = 'mksquashfs'
        self.thread_start(cmd)


    # 4. run mksquashfs command
    def mksquashfs(self):
        self.update_copy_info_label(4)
        self.builder.get_object("copy_spinner").show()
        self.builder.get_object("copy_progress_bar").set_text('Making squash filesystem...')

        squashfs_filepath = self.build_dir + '/filesystem.squashfs'
        cmd = '/usr/bin/mksquashfs ' + self.mnt_dir + ' ' + squashfs_filepath + ' -noappend'
        cmd = 'script --flush --quiet --command "' + cmd + '"' # needs to be wrapped so we can read progress acurately since stdbuf -o0 didnt work
        print('exec', cmd)
        
        self.type_progress_bar = 'mksquashfs'
        GLib.timeout_add(300, self.update_progress_bar)
        self.run_after_thread = 'make_iso'
        self.thread_start(cmd)


    # 6. make iso
    def make_iso(self):
        self.update_copy_info_label(5)
        print('make iso')

        GLib.timeout_add(300, self.make_iso_progress_bar)

        # make iso via iso-level 3 so its size isnt restricted to 4.8G (Dvd size)
        # and use compiled xorriso in our dir as it was missing on my system (@todo test if exists before include)
        iso_target = self.builder.get_object("backup_filepath").get_text()
        xorriso_filepath = os.path.abspath(os.path.dirname(__file__)) + '/xorriso'
        cmd = "grub-mkrescue --xorriso=" + xorriso_filepath + " --iso-level 3 -o " + iso_target  + " " + self.build_dir
        cmd = 'script --flush --quiet --command "' + cmd + '"'
        self.run_after_thread = 'finished'
        self.thread_start(cmd)
        

    # chain is finished, hide copy window and inform user
    def finished(self):
        print('finished! opening dir with iso...')
        self.builder.get_object("copy_window").hide()
        iso_target = self.builder.get_object("backup_filepath").get_text()
        iso_dir = os.path.dirname(iso_target)
        cmd = 'pcmanfm ' + iso_dir
        self.thread_start(cmd)


    # parse output so we can animate the progress bar (@todo, merge with update_progress_bar)
    def make_iso_progress_bar(self):
        if self.stop_progress_bar:
            print('timer should stop')
            self.stop_progress_bar = False
            return False

        bar = self.builder.get_object("copy_progress_bar")

        try: line = self.queue.get_nowait()
        except Empty:
            return True
        else:
            line = line.strip()

            if not line or line.find(': UPDATE :') == -1 or line.find('done') == -1:
                return True

            self.builder.get_object("copy_spinner").hide()

            tmp = line.split(': UPDATE :')
            parts = tmp[1].split('done')
            percent = parts[0].strip()

            percent = percent.replace('%', '')
            fraction = float(percent) / 100
            bar.set_fraction(fraction)
            bar.set_text(percent + '% done...')

        return True


    # close is clicked so kill pid
    def on_copy_window_destroy(self, window):
        print('killing copy pid', self.copy_pid)
        os.killpg(os.getpgid(self.copy_pid), signal.SIGTERM)
        window.hide()


if __name__ == "__main__":
    app = Friso()
    Gtk.main()
