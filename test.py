#!/usr/bin/env python3

from subprocess import Popen, PIPE, STDOUT

cmd = "rsync -a --info=progress2 /mnt/fedora /tmp/testfolder | tr '\r' '\n' | awk '{print $2}'"

p = Popen(cmd, stdout = PIPE,
        stderr = STDOUT, text = True, shell = True)

for line in p.stdout:
    percent = line.replace('%', '').strip()
    print("wa:", percent)
